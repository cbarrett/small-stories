Title: Two Words
Author: Colin Barrett

Poles dot towers and turrets like tittles above *i*s and *j*s. Flags run up the poles, unfurling and waving like rugs being beaten out on balconies. Tom walks among the assembled revelers and feels the giddy, gay energy flow into him like a wine into a carafe.

Tom leaps on to a low stage as he passes and grabs a microphone. His skin is ruddy and sunkissed. His face sports a short brambly stubble. He’s wearing dark linen slacks, boots that are an old tire grey, chunky and surefooted, a turtleneck sweater with a puffy, thick weave like barbed wire, and inky brown sunglasses. His nostrils are flared as big as a bull's. His mouth open wide in a grin. The grin becomes a shout, two words long. The microphone and PA system turn his two words from sound to electricity, back to sound again, and when they reach the crowd they turn back into electricity once more, running up and down the crowd's nerves like Christmas morning six year olds.

"Happy birthday!"