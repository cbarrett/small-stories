Title: Young Palms
Author: Colin Barrett

“Trilbee? Trilaboo! Trilyou,” Hester says, alternating mysterious and excited inflections. He hops back and forth from one foot to the other and his jester bells rattle, echoing in the cave.

“Trila tahdo tahdo tahday.” Lester pantomimes an aloof Frenchman, complete with cigarette and beret.

“I-I’m looking for something.” Dave keeps his adolescent voice from cracking, but fails to notice that his fists have rolled up into anxious geoids. Exhale, Dave, he reminds himself. “The stone.” His fists unroll like small ferns. “I know you have it and I’m here to take it back.” His voice is glassy and his eyes are bright.

Lester and Hester both turn towards him. They are terrifying mirror images, dressed in navy and dandelion respectively. Their jester costumes are otherwise identical, as are they themselves. Both have smooth, primer white skin marred only by two huge diamonds splotched on the skin around their shallow set eyes. Neither have any visible hair, not even eyelashes. Their lips are grey, wrinkled lines around their mouths, both of which are currently open and shaped in surprise.

“Oooohhhh!” coos Hester. Lester cackles. Hester shows Dave his empty palm, does a flourishing bow, and produces a stone, seemingly from nowhere at all. He mugs at Lester and they laugh silently together. Dave reaches for it, but Hester draws back and his face stretches into a wicked grin. It won’t be that easy, you’ll have to play with me first, is what Dave supposes he’s thinking. Dave lunges, putting all his weight on his frontmost foot, making a speedy grab. But Hester tosses the stone over Dave’s head to his blue besarted companion. Dave finds his balance and glares at Hester, who shrugs, shakes his head and laughs as if to say I have no idea, who can say where it is now?

“It’s mine!” Dave is surprised by his voice’s ursine ferocity. He turns to Lester. “You must give it to me.” Lester pouts, kicking an imaginary can down the road, but nonetheless raises his weird white palm with the stone resting on it. Dave snatches it. The stone is smooth and cool. It looks like an ordinary cobblestone, or at least what Dave supposes an ordinary cobblestone would look like anyway.

He puts the stone between his young palms and closes his eyes. The cave, heretofore lit only by a small brazier, begins to brighten. Shadows fade away. A draft from nowhere in particular pushes Dave’s hair upwards and it flutters like a flag. Suddenly, there is a burst of light from behind Hester and Lester, who swivel and gaze, piscine and open-mouthed. A shimmering portal has appeared, heat mirage wavy, and at its center is a woman. She is supremely vernal, like a huge oak tree whose buds are opening together time lapse quick. Her eyes are misty lakes at dawn full of leaping, eager trout. Her hair is fresh rivulets of sap, dripping past her shoulders. Her cheeks are as smooth as a freshly laid egg. And her nose might as well be the Easter Bunny’s tail.

“Mother.” Dave’s voice is soft and innocent, his eyes shiny and wet. “At last I’ve found you.” He walks between the gawping pair of cave-dwellers and into her arms, which have spread like flowers turning towards the sun.