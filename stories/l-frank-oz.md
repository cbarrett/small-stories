Title: L. Frank Oz
Author: Colin Barrett

Cigarette smoke, radiator air, and snowy cold swirl together in the cabin of Frank’s car. He feels like he’s being blended into a frappuccino, warm coffee and milk losing heat to ice as they dissolve together, transmuting from reactants into product. Frank reflects on the alchemical nature of cooking as he drives; it is a welcome distraction.

Frank’s car speeds down an empty Minnesota highway, sliding atop the embankment like a skateboard on an endless grind. The car turns off the road and into a parking lot shared by a bank and a fast food restaurant. In the cabin, Frank turns off the car and fumbles for his belongings.  The parking lot is full enough to seem lively but not so full as to impede parking; somewhere, its designer smiles. Gear assembled, Frank takes a deep breath, pulls on a balaclava, cocks his pistol, and steps out of the car.

“Nobody move! Everyone up against that wall!” Frank instructs the bank’s patrons without fear in his voice. The security cameras silently look on, turning this singular, unusual event into mere magnetic transitions on a hard disk. Frank’s mind is placid but memories of his father play unbidden across its surface like dragon flies alighting on water lilies. The feeling of his father’s strong arms holding young Frank close against his sweaty torso during the dampest, hottest part of summer, reading “The Wizard of Oz” aloud.

“I want you to turn to the person next to you,” now Frank’s voice begins to waver and crack, “and give them a hug!”
