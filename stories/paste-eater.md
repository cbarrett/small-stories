Title: Paste Eater
Author: Colin Barrett

You are at the dentist when something strange begins to happen.

At first you attribute it to nerves. Not the physical ones in your teeth, but the proverbial ones which you imagine spiderweb through your whole body, limbic, and which from time to time tug at you and keep you from going certain places or doing certain things. But who is doing the tugging? A guardian angel? A puppeteer? You’ve never been sure.

It soon becomes clear, however, that it’s not just nerves. You are sinking into chair, really physically sinking into it, becoming part of it, its torn plastic becoming the texture of your skin. Now you are seeing yourself from above. You don’t look very good. Worse than usual.

You have died.

Really? you think. You’re not sure what to make of it all. Death, wow. You thought it would be different.
You feel like you should be thinking things like “I’m not ready” or “I wasn’t done.” Or alternately feeling a sense of peace or acceptance or something. You half expect to see some sort of ghostly miasma, a sprit from the beyond to guide you. They would have a friendly, androgynous face. Very pleasing. Offensive to nobody.

But that isn’t what you’re feeling at all. You feel regret. A great whirlpool of regret, spinning into nothingness at the center of yourself. 

Bullshit, you think, surprising yourself. When you were alive that’s all you felt. Regret, regret, regret. You messed things up with that girl. You didn’t pay your bills on time. You didn’t cook enough. You spent too much time with people you didn’t like and not enough with people you did.

The truth of it is, you chose to die to escape regret. You don’t know how you forgot that. It just seemed easier. Those limbic threads steered you straight into the great unknown, down into that whorl where your you should be but never was and you followed them right down in like Boba Fett, down into the Great Pit of Karkoom, down into Sarlacc’s mouth.

This is bullshit, you scream into a darkness so infinite it is beyond perfect, so perfect it is slapdash, it is an arts & crafts project, it is elbow macaroni and Elmer’s glue on laser paper. And at the center you see, you feel, the you-shaped deboss into which you have been pressed. And then you feel whatever was pressing you, holding you here, lift. And then you slide away.