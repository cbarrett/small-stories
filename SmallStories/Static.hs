{-# LANGUAGE TemplateHaskell #-}
module SmallStories.Static
  ( staticRoutes
  , staticPages
  ) where

import qualified Data.ByteString.Char8 as B
import Data.Char (toLower)
import Data.Functor
import Data.Text (pack)
import Control.Monad
import Language.Haskell.TH
import Language.Haskell.TH.Syntax
import System.Directory
import System.FilePath
import System.Posix.Files

staticRoutes :: FilePath -> Q [Dec]
staticRoutes p = fmap concat $ files p $ \i p' -> do
  addDependentTree p' p False
  let body = normalB $ conE (mkName "StaticR") `appE` ([|pack|] `appE` liftString (makeRelative p p'))  
  let iN = mkName ("route_" ++ i)
  sequence [ sigD iN (conT (mkName "Route"))
           , funD iN [clause [] body []]]

staticPages :: FilePath -> Q Exp
staticPages p = fmap ListE $ files p $ \_ p' ->
  let readFile' f = addDependentTree f p True >> runIO (B.readFile f)
  in conE (mkName "StaticP")
     `appE` ([|pack|] `appE` liftString (makeRelative p p')) 
     `appE` ([|B.pack|] `appE` (stringE . B.unpack =<< readFile' p'))

addDependentTree :: FilePath -> FilePath -> Bool -> Q ()
addDependentTree p base inclFile = do
  absBase <- runIO $ canonicalizePath base
  let p' = if not inclFile then dropFileName p else p
  let paths = scanl (</>) absBase (splitDirectories (makeRelative' base p'))
  mapM_ addDependentFile paths  -- We add every subdirectory along `p` in the hopes of catching any new files added therein
  where makeRelative' x y = let r = makeRelative x y in if r == "." then "" else r

files :: FilePath -> (String -> FilePath -> Q a) -> Q [a]
files p f = do
  subs <- runIO $ allSubpaths p
  let ids = identifierName <$> subs
  let paths = (p </>) <$> subs
  zipWithM f ids paths

allSubpaths :: FilePath -> IO [FilePath]
allSubpaths p = allSubpaths' p p

allSubpaths' :: FilePath -> FilePath -> IO [FilePath]
allSubpaths' base p = do
  contents <- getDirectoryContents p
  c' <- contents `forM` \p' -> do
    let absolutePath = p </> p'
    let relativePath = makeRelative base absolutePath
    isDir <- pathIsDirectory absolutePath
    if p' == "." || p' == ".." then
      return []
    else if isDir then
      allSubpaths' base absolutePath
    else
      return [relativePath]
  return $ concat c'

pathIsDirectory :: FilePath -> IO Bool
pathIsDirectory p = do
  s <- getFileStatus p
  return $ isDirectory s

identifierName :: FilePath -> String
identifierName = fmap go
  where go c
          | 'A' <= c && c <= 'Z' = toLower c
          | 'a' <= c && c <= 'z' = c
          | '0' <= c && c <= '9' = c
          | otherwise = '_'