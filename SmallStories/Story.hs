module SmallStories.Story
  ( Story(..)
  , storySlug
  , storyPandoc
  , storyPandocs
  , renderTitle
  ) where

import Control.Applicative
import Control.Monad
import Data.Functor
import Data.List
import qualified Data.Map as M
import Data.Monoid
import Data.Text hiding (zip, zipWith)
import Text.Pandoc
import System.FilePath

data Story = LFrankOz
           | YoungPalms
           | PasteEater
           | TwoWords
           deriving (Show, Eq, Ord, Enum, Bounded)

-- Can we automate this with generics?
storySlug :: Story -> String
storySlug LFrankOz   = "l-frank-oz"
storySlug YoungPalms = "young-palms"
storySlug PasteEater = "paste-eater"
storySlug TwoWords   = "two-words"

storyPandoc :: FilePath -> Story -> IO Pandoc
storyPandoc storiesDir s = readFile filePath >>= return . parseMarkdown
  where filePath = storiesDir </> (storySlug s) `addExtension` ".md"
        parseMarkdown = readMarkdown def { readerExtensions = multimarkdownExtensions }

storyPandocs :: FilePath -> IO (M.Map Story Pandoc)
storyPandocs storiesDir = msum' $ zipWith build allStories allPandocActions
  where msum' = (liftM mconcat) . sequence
        build s = liftM $ M.singleton s
        allStories = [minBound..]
        allPandocActions = (storyPandoc storiesDir) <$> allStories

renderTitle :: Pandoc -> Text
renderTitle (Pandoc (Meta title _ _) _) = pack $ writePlain def (Pandoc emptyMeta [Plain title])
  where emptyMeta = Meta mempty mempty mempty