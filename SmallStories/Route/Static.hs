{-# LANGUAGE TemplateHaskell #-}
module SmallStories.Route.Static where

import SmallStories.Static (staticRoutes)
import SmallStories.Route

$(staticRoutes "static")
