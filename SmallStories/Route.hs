{-# LANGUAGE OverloadedStrings #-}
module SmallStories.Route
  ( Route(..)
  , renderRoute
  ) where

import Data.Text
import SmallStories.Story

data Route = IndexR
           | AboutR
           | StoryR !Story
           | StaticR !Text

renderRoute :: Route -> [(Text, Text)] -> Text
renderRoute r _ = go r
  where go IndexR = "index.html"
        go AboutR = "about.html"
        go (StoryR story) = (pack $ storySlug story) `append` ".html"
        go (StaticR t) = t
