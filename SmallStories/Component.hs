{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses, OverloadedStrings, TemplateHaskell, TypeSynonymInstances, QuasiQuotes #-}
{-# LANGUAGE GADTs #-} -- This is to enable the identiy constraints for instances below

-- Credit where it is due: much of this is cribbed from the brilliant Yeson.Widget
module SmallStories.Component
  ( Component
  , ToComponent(..)
  , setTitle
  , chamlet
  , chamletFile
  , renderComponent
  , PageContent(..)
  ) where

import Debug.Trace
import SmallStories.Route

import Control.Monad
import Data.Maybe
import Data.Monoid
import Data.Text (Text, unpack)
import qualified Data.Text.Lazy as TL
import qualified Control.Monad.Reader as R
import qualified Control.Monad.Writer.Lazy as W
import Language.Haskell.TH
import Language.Haskell.TH.Quote (QuasiQuoter)
import Text.Blaze.Html
import Text.Blaze.Html.Renderer.String (renderHtml)
import Text.Hamlet
import Text.Lucius

type Render r = r -> [(Text, Text)] -> Text
data CompConfig a = CompConfig { ccRender :: Render a }

data CompData a = CompData { cdHtml  :: HtmlUrl a
                           , cdCss   :: ![CssUrl a]
                           , cdTitle :: !(Last Text) }
instance Monoid (CompData a) where
  mempty = CompData mempty mempty mempty
  mappend (CompData a1 a2 a3) (CompData b1 b2 b3) = 
    CompData (a1 `mappend` b1)
             (a2 `mappend` b2)
             (a3 `mappend` b3)

type Component r = R.ReaderT (CompConfig r) (W.Writer (CompData r))

class ToComponent r a where
  toComponent :: a -> Component r ()

-- instance for HtmlUrl
instance render ~ Render r => ToComponent r (render -> Html) where
  toComponent h = W.tell $ CompData h mempty mempty

-- instance for CssUrl
instance render ~ Render r => ToComponent r (render -> Css) where
  toComponent c = W.tell $ CompData mempty [c] mempty

instance r ~ r' => ToComponent r (Component r' ()) where
  toComponent = id

instance ToComponent r Html where
  toComponent = toComponent . const

setTitle :: Text -> Component r ()
setTitle t = W.tell $ CompData mempty mempty (Last (Just t))

chamlet = hamletWithSettings rules defaultHamletSettings
chamletFile = hamletFileWithSettings rules defaultHamletSettings

rules :: Q HamletRules
rules = do
  tc <- [|toComponent|]
  let ur f = f $ Env { urlRender = Just $ \g -> do
                        x <- newName "urender"
                        let m = lam1E (varP x) (g $ VarE x)
                        infixApp [|return ccRender `ap` R.ask|] [|(>>=)|] m
                     , msgRender = Nothing }
  return $ HamletRules tc ur $ \_ e -> return $ tc `AppE` e

data PageContent r = PageContent { pcHtml :: HtmlUrl r
                                 , pcCss :: !TL.Text
                                 , pcTitle :: !Text }

renderComponent :: (PageContent r -> HtmlUrl r) -> Component r a -> Render r -> String
renderComponent f c r =
  let cd = W.execWriter $ R.runReaderT c (CompConfig r)
      css = mconcat $ fmap (renderCssUrl r) $ cdCss cd
      title = (fromMaybe "" . getLast . cdTitle) cd
      htmlUrl = f $ PageContent (cdHtml cd) css title
  in renderHtml (htmlUrl r)
