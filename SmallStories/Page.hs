{-# LANGUAGE TemplateHaskell #-}
module SmallStories.Page
  ( Page(..)
  , pageFilePath
  , allStaticPages
  , allStoryPages
  ) where

import qualified Data.ByteString as B
import Data.Functor
import Data.Map (Map, (!))
import Data.Text
import Text.Pandoc
import SmallStories.Static (staticPages)
import SmallStories.Story

-- I think we can elimimate the page type completely!
-- The only thing we need is a place to store filename and data of static files and that can just be in Static.hs itself
data Page = IndexP
          | AboutP
          | StoryP !Story !Pandoc
          | StaticP !Text !B.ByteString

pageFilePath :: Page -> FilePath
pageFilePath (IndexP) = "index.html"
pageFilePath (AboutP) = "about.html"
pageFilePath (StoryP s _) = (storySlug s) ++ ".html"
pageFilePath (StaticP t _) = unpack t

allStaticPages :: [Page]
allStaticPages = $(staticPages "static")

allStoryPages :: Map Story Pandoc -> [Page]
allStoryPages ps = (\s -> StoryP s (ps ! s)) <$> [minBound..]
