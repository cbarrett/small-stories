{-# LANGUAGE OverloadedStrings, QuasiQuotes #-}
module SmallStories.Component.Story (storyComponent) where

import Data.Monoid
import Data.Text
import SmallStories.Component
import SmallStories.Component.Header
import SmallStories.Route
import SmallStories.Story
import Text.Blaze.Html
import Text.Cassius
import Text.Pandoc

storyComponent :: Pandoc -> Component Route ()
storyComponent p = do
  setTitle $ (renderTitle p) <> " | Small Stories"
  [chamlet|
    ^{headerComponent}
    <h3>#{renderTitle p}
    <hr>
    ^{writeHtml def p}
    <hr>|]
  toComponent [cassius|
    p
      text-align: justify
      margin-bottom: 0
    p + p
      text-indent: 1.5em
    hr
      background-color: #b8b9cb
      border: 0
      border-bottom: 1px solid #f6f6fc
      margin: 1.5em auto
      width: 120px
      height: 1px|]