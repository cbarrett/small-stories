{-# LANGUAGE QuasiQuotes #-}
module SmallStories.Component.Header
  (headerComponent
  ) where

import SmallStories.Component
import SmallStories.Route
import Text.Hamlet
import Text.Cassius

headerComponent :: Component Route ()
headerComponent = do
  [chamlet|$newline never
    <h1><a href="@{IndexR}">Small Stories</a>
    <h2><a href="@{IndexR}">by Colin Barrett</a>
    <div #tagline>A growing collection of very short stories on the web. <a href="@{AboutR}">More&hellip;</a>|]
  toComponent [cassius|
    h1
      font-style: italic
      font-size: 72pt
      padding-top: .5em
      text-shadow: 0px 2px 1px white
    h1 a
      color: black
      text-decoration: none
    h2
      font-style: italic
      font-size: 36pt
      text-align: right
      text-shadow: 0px 1px 0px white
    h2 a
      color: black
      text-decoration: none
    #tagline
      text-align: center
      font-style: italic
      margin: 13px 0 60px 0
      font-size: 15pt
      text-shadow: 0px 1px 0px white|]
