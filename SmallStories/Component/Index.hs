{-# LANGUAGE OverloadedStrings, QuasiQuotes #-}
module SmallStories.Component.Index
  (indexComponent
  ) where

import qualified Data.Text as T
import qualified Data.Map as M
import Data.Map ((!))
import SmallStories.Component
import SmallStories.Component.Header
import SmallStories.Route
import SmallStories.Story
import Text.Cassius
import Text.Pandoc

-- route, title, count
indexComponent :: M.Map Story Pandoc -> Component Route ()
indexComponent ps = do
  setTitle "Small Stories"
  [chamlet|$newline never
    ^{headerComponent}
    <dl>
      ^{story LFrankOz}
      ^{story TwoWords}
    <dl>
      ^{story YoungPalms}
      ^{story PasteEater}
    <div #clear>|]

  toComponent [cassius|
    dl
      width: 100%
    dl + dl
      clear: left
      padding-top: 1.25em
    dt
      float: left
      width: 80%
      overflow: hidden
      white-space: nowrap
    dd
      float: left
      width: 20%
      overflow: hidden
    dt:after
      content: "#{T.replicate 66 " ."} "
    dd
      margin-bottom: 2em
    #clear
      clear: both|]
  
  where story s = indexStoryComponent s (ps ! s)

indexStoryComponent :: Story -> Pandoc -> Component Route ()
indexStoryComponent s p = [chamlet|$newline never
  <dt><a href="@{StoryR s}">#{renderTitle p}</a>
  <dd>#{show (wordCount p)} words|]

wordCount :: Pandoc -> Int
wordCount = rnd 50 . length . words . (writePlain def)
  where rnd f n = max f $ f * round (fromIntegral n / fromIntegral f)
