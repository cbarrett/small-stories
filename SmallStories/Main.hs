{-# LANGUAGE DeriveDataTypeable, QuasiQuotes #-}
module Main where

import Control.Monad
import qualified Data.ByteString as B
import qualified Data.Map as M
import Data.Map ((!))
import Data.Functor ((<$>))
import Data.Maybe
import Data.Monoid (mempty)
import Data.Text (pack, Text)
import Text.Blaze.Html (preEscapedToHtml)
import Text.Hamlet (hamlet)
import Text.Pandoc as P
import SmallStories.Component
import SmallStories.Component.Index
import SmallStories.Component.About
import SmallStories.Component.Story
import SmallStories.Page
import SmallStories.Route
import SmallStories.Route.Static
import SmallStories.Story
import System.Console.CmdArgs as C
import System.Directory
import System.FilePath

writeComponent :: FilePath -> Component Route () -> IO ()
writeComponent p c = writeFile p $ renderComponent layout c renderRoute
  where layout pc = [hamlet|$newline never
                      <html>
                        <head>
                          <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                          <title>#{pcTitle pc}
                          <link rel="stylesheet" type="text/css" href="@{route_reset_css}">
                          <link rel="stylesheet" type="text/css" href="@{route_style_css}">
                          <style>#{preEscapedToHtml (pcCss pc)}
                          <script type="text/javascript" src="//use.typekit.net/pnq2kqg.js"></script>
                          <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
                        <body>^{pcHtml pc}|]


 
fixupDir :: FilePath -> Bool -> IO FilePath
fixupDir dir create = do
  when create $ createDirectoryIfMissing True dir
  canonicalizePath dir    

buildAction :: Command -> IO ()
buildAction options = do
  outputDir <- fixupDir (output options) True
  storiesDir <- fixupDir (stories options) False
  storyMap <- storyPandocs storiesDir
  
  -- Write components
  writeComponent (pfp outputDir IndexP) $ indexComponent storyMap
  writeComponent (pfp outputDir AboutP) $ aboutComponent
  allStoryPages storyMap `forM_` \page@(StoryP _ pandoc) -> do
    writeComponent (pfp outputDir page) $ storyComponent pandoc
  
  -- Write static pages
  mapM_ (writeStaticPage outputDir) allStaticPages
  
  where pfp o p = o </> pageFilePath p
        writeStaticPage o p@(StaticP _ b) = B.writeFile (o </> pageFilePath p) b

data Command = Build { output :: String
                     , stories :: String }
  deriving (Show, Data, Typeable)

build = Build { output  = "output" &= typDir &= help "Output directory. Defaults to \"output\""
              , stories = "stories" &= typDir &= help "Stories directory. Defaults to \"stories\""
              } &= help "build static site files"
              
main :: IO ()
main = do
  options <- cmdArgs $ modes [build] &= program "smallstories"
  case options of
    Build _ _ -> buildAction options
