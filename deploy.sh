#!/bin/bash -e

echo "*** Installing..."
cabal-dev install

echo "*** Generating..."
./cabal-dev/bin/smallstories

# XXXcb Ideally this should upload to temporary directory and then move it or update a symlink or something if the upload succeeds
echo "*** Uploading..."
rsync -avz output/ smallstories@smallstories.org:smallstories.org
